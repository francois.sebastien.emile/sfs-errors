/**
 * Class to extend to add more business informations to your response.
 * @method toString
 */
export class BaseError extends Error {

  constructor(message?: string) {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  }

  public toString(): string {
    return JSON.stringify({
      message: this.message,
      name: this.name,
      stack: this.stack,
    });
  }
}
