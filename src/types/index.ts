export type ErrorConstructor<T extends Error> = new(...args: any[]) => T;

export class ErrorDescriptor {
    constructor(public readonly code: number, public readonly errorType: ErrorConstructor<Error>) {}
}
