import { ErrorHandler } from "../handler/ErrorHandler";
import { ErrorDescriptor } from "../types";

const errorHandler: ErrorHandler = new ErrorHandler();

export function ErrorHandling(...errors: ErrorDescriptor[]) {
    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        const originalMethod = descriptor.value;

        descriptor.value = function() {
            const context = this;
            const args = arguments;
            const response = args[1];
            try {
                originalMethod.apply(context, args);
            } catch (error) {
                errorHandler.handleError(response, error, ...errors);
            }
        };

        return descriptor;
    };
}
