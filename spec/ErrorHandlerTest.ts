import { IncomingMessage, ServerResponse } from "http";
import { Socket } from "net";
import { BaseError } from "../src/error/BaseError";
import { ErrorHandler } from "../src/handler/ErrorHandler";

describe("ErrorHandler implementation tests", () => {

    const errorMessage = "Test error message";
    const responseFakeObject = new ServerResponse(new IncomingMessage(new Socket()));

    let errorHandler: ErrorHandler;

    beforeEach(() => {
        errorHandler = new ErrorHandler();
    });

    describe("manageErrorResponse()", () => {
        it("should return given BaseError when BaseError instance is provided with error message", () => {
            // given
            const error = new BaseError(errorMessage);
            spyOn(responseFakeObject, "setHeader");
            spyOn(responseFakeObject, "end");

            // when
            errorHandler.handleError(responseFakeObject, error);

            // then
            expect(responseFakeObject.setHeader).toHaveBeenCalledTimes(2);
            expect(responseFakeObject.setHeader).toHaveBeenCalledWith("Content-Type", "application/json");
            expect(responseFakeObject.statusCode).toEqual(500);
            expect(responseFakeObject.end).toHaveBeenCalledTimes(1);
            expect(responseFakeObject.end).toHaveBeenCalledWith(error.toString(), "utf8");
        });
        it("should send response with 500 status code when BaseError instance is provided w/o error message", () => {
            // given
            const error = new BaseError();
            spyOn(responseFakeObject, "setHeader");
            spyOn(responseFakeObject, "end");

            // when
            errorHandler.handleError(responseFakeObject, error);

            // then
            expect(responseFakeObject.setHeader).toHaveBeenCalledTimes(2);
            expect(responseFakeObject.setHeader).toHaveBeenCalledWith("Content-Type", "application/json");
            expect(responseFakeObject.statusCode).toEqual(500);
            expect(responseFakeObject.end).toHaveBeenCalledTimes(1);
            expect(responseFakeObject.end).toHaveBeenCalledWith(error.toString(), "utf8");
        });
        it("should send response with 400 status code when managed error types are given", () => {
            // given
            const error = new BaseError();
            const managedErrorTypes = [{ code: 400, errorType: BaseError }];
            spyOn(responseFakeObject, "setHeader");
            spyOn(responseFakeObject, "end");

            // when
            errorHandler.handleError(responseFakeObject, error, ...managedErrorTypes);

            // then
            expect(responseFakeObject.setHeader).toHaveBeenCalledTimes(2);
            expect(responseFakeObject.setHeader).toHaveBeenCalledWith("Content-Type", "application/json");
            expect(responseFakeObject.statusCode).toEqual(400);
            expect(responseFakeObject.end).toHaveBeenCalledTimes(1);
            expect(responseFakeObject.end).toHaveBeenCalledWith(error.toString(), "utf8");
        });
        it("should send response with 500 status code when provided error instance is not BaseError instance", () => {
            // given
            const error = new Error(errorMessage);
            spyOn(responseFakeObject, "setHeader");
            spyOn(responseFakeObject, "end");

            // when
            errorHandler.handleError(responseFakeObject, error);

            // then
            expect(responseFakeObject.statusCode).toEqual(500);
            expect(responseFakeObject.setHeader).toHaveBeenCalledTimes(2);
            expect(responseFakeObject.setHeader).toHaveBeenCalledWith("Content-Type", "application/json");
            expect(responseFakeObject.end).toHaveBeenCalledTimes(1);
        });
    });

});
