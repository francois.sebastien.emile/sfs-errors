import { ServerResponse } from "http";
import { BaseError } from "../error/BaseError";
import { ErrorDescriptor } from "../types";

/**
 * Handler used to standardize behaviour of error handling for client purpose.
 * @method handleError
 */
export class ErrorHandler {

  /**
   * Method to use to manage error response.
   * @param response
   * @param error
   */
  public handleError(response: ServerResponse, error: Error, ...managedErrorTypes: ErrorDescriptor[]): void {

    const finalError = this.castToBaseError(error);
    let httpCode: number | undefined;

    if (managedErrorTypes) {
      httpCode = this.getManagedErrorHttpResponseCode(error, managedErrorTypes);
    }

    this.manageServerErrorResponse(response, finalError, httpCode);

  }

  /**
   * Returns new BaseError instance based on the given Error instance.
   * @param error
   */
  private castToBaseError(error: Error): BaseError {
    if (!(error instanceof BaseError)) {
      return new BaseError(error.message);
    } else {
      return error;
    }
  }

  /**
   * Returns associated HTTP status code if given Error is an instance of the managed error types given in parameters,
   * undefined it is not a managed error.
   * @param error
   * @param managedErrorTypes
   */
  private getManagedErrorHttpResponseCode(error: BaseError, managedErrorTypes: ErrorDescriptor[]): number | undefined {
    const matchingErrorTypeElement = managedErrorTypes.find((value) => error instanceof value.errorType);
    return (matchingErrorTypeElement) ? matchingErrorTypeElement.code : undefined;
  }

  /**
   * Set response 'Content-Type', 'Content-Length' and 'Encoding' headers and then send server response from the given
   * parameters. The error will serve as response body.
   * @param response
   * @param error
   * @param httpCode
   */
  private manageServerErrorResponse(response: ServerResponse, error: BaseError, httpCode?: number): void {
    const body = error.toString();
    response.setHeader("Content-Type", "application/json");
    response.setHeader("Content-Length", body.length);
    response.statusCode = httpCode || 500;
    response.end(body, "utf8");
  }

}
